var mongoose    = require('mongoose');
var log         = require('./log')(module);
var config = require('config');
mongoose.connect(config.get('Mongoose.uri'));
var db = mongoose.connection;
db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback () {
    log.info("Connected to DB!");
});

var Schema = mongoose.Schema;

// Schemas

var Task = new Schema({
    description: { type: String, required: true },
    modified: { type: Date, default: Date.now },
    complete: {type: Boolean, default: false},
    user: { type: Schema.Types.ObjectId, required: false }
});

var TaskModel = mongoose.model('Task', Task);

module.exports.TaskModel = TaskModel;